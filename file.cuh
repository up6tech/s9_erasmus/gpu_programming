//
// Created by fabien on 13/09/2022.
//
#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <fstream>

#ifndef CLION_WORKSPACE_FILE_CUH
namespace fi {
    /**
     *
     * @param filename File name
     * @return  The data set as a list of tuple of float (ascention, descending)
     */
    std::vector <std::tuple<float, float>> load_csv(std::string filename);

    /**
     *
     * @param filename File name
     * @param bin_data The histogram data
     * @param bin_count Number of bin inside histogram
     * @return
     */
    unsigned long long int write_csv(std::string filename, unsigned long long int* bin_data, int bin_count);
}
#define CLION_WORKSPACE_FILE_CUH

#endif //CLION_WORKSPACE_FILE_CUH
