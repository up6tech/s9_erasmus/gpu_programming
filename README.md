# GPU Programming

The aim of this project is to *"prove"* the presence of dark matter in the universe,
by comparing a synthetic random evenly distributed set of 100 000
galaxies and (**R**) and a measured set of 100 000 galaxies (**D**).

## Requirements
- python >= 3.8
    - venv / virtualenv / pipenv / conda
- c++ compiler >= 11
- CMake >= 3.17
- CUDA

## Installation

python setup (VENV)
```shell
python -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

cmake
```shell
cmake --install .
```

## Build
```shell
cmake --build .
```