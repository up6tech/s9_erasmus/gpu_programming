#include <iostream>
#include <chrono>
#include <assert.h>
#include "file.cuh"
#define m_debug std::cout << "Debug at " << __LINE__ << " of file " << __FILE__ << std::endl
#define floatCount 2
#define histogramCount 3
#define dataSetCount 2
#define PI 3.14159f
#define bin_width 0.250f
#define bin_count 720

/**
 * Use long long int for indexing data (search)
 * atomicAdd() for bin
 *
 * @param bin Histogram output
 * @param input Data set
 * @param dataSize Size of one of the data set
 * @param input_size Size of one data set (100 000)
 */
__global__ void kernelCompute(
    unsigned long long int *bin, const float *input, long long int dataSize, long long int input_size)
{
  long long int floatCount__ = 2;
  long long int dataSetCount__ = 2;
  long long int histogramCount__ = 3;
  long long int dataSize__ = 100000;
  // const values

  if ((long long int)blockIdx.x < histogramCount__)
  {
    long long int globalId = (long long int)threadIdx.x +
                             (long long int)blockIdx.z * blockDim.x +
                             (long long int)blockIdx.y * blockDim.x * gridDim.z;

    // 10 000 000 000
    if (globalId < dataSize__ * dataSize__)
    {
      long long int shift = ((long long int)blockIdx.x) * dataSize__ * dataSetCount__;

      long long int galaxy1 = ((globalId / dataSize__) + shift) * 2;
      long long int galaxy2 = ((globalId % dataSize__) + shift + dataSize__) * 2;

      float a1, a2, d1, d2;

      a1 = input[galaxy1] * ((1 / 60.0f * PI / 180.0f));     // from arcmin to radian
      d1 = input[galaxy1 + 1] * ((1 / 60.0f * PI / 180.0f)); // from arcmin to radian

      a2 = input[galaxy2] * ((1 / 60.0f * PI / 180.0f));     // from arcmin to radian
      d2 = input[galaxy2 + 1] * ((1 / 60.0f * PI / 180.0f)); // from arcmin to radian

      // angle between two galaxies
      // q12 = arccos(sin(d1)*sin(d2)+cos(d1)*cos(d2)*cos(a1-a2))
      float farg = sin(d1) * sin(d2) + cos(d1) * cos(d2) * cos(a1 - a2);

      if (farg > 1.0f)
        farg = 1.0f;
      if (farg < -1.0f)
        farg = -1.0f;

      float radian = acos(farg);

      // convert to degree
      float degree = radian * (180.0f / PI);

      unsigned int binIndex = (unsigned int)floorf(degree / bin_width);
      if (binIndex < bin_count)
      {
        unsigned int globalBinIndex = blockIdx.x * 720U + binIndex;
        if (globalBinIndex < 3U * 720U)
        {
          // put in bin (SYNCHRONIZED ACTION)
          // atomicInc(&bin[globalBinIndex], 10000000LLU);
          atomicAdd(&bin[globalBinIndex], 1LLU);
        }
      }
    }
  }
}

/**
 * Use CPU to compute same calculation
 * @param array_index
 * @param histogramIndex
 * @param bin
 * @param input
 * @param dataSize
 * @param bin_count
 */
void cpuCompute(
    long long int lBlockIdx, long long int lBlockIdy, long long int lBlockIdz,
    long long int gridDimZ, long long int blockDimX, long long int gridDimX,
    long long int lThreadIdx,
    unsigned long long int *bin, const float *input, long long int dataSize, long long int input_size)
{
  // const values
  long long int histogramIndex = lBlockIdx;
  long long int dataSize__ = dataSize;
  long long int floatCount__ = 2;
  long long int floatSize__ = dataSize__ * floatCount__;

  if (histogramIndex < histogramCount)
  {
    long long int localDataSize = dataSize;

    long long int globalId = (long long int)lThreadIdx +
                             (long long int)lBlockIdz * blockDimX +
                             (long long int)lBlockIdy * blockDimX * gridDimZ;

    if (globalId >= localDataSize * localDataSize)
    {
      return;
    }

    long long int shift = ((long long int)lBlockIdx) * 400000LL;

    long long int galaxy1 = ((globalId / dataSize__) + shift) * 2;
    long long int galaxy2 = ((globalId % dataSize__) + shift + dataSize__) * 2;

    if (galaxy1 % 2 != 0 || galaxy2 % 2 != 0)
    {
      std::cerr << "Error galaxy1 = " << galaxy1 % 2 << " and galaxy2 = " << galaxy2 % 2 << " when" << std::endl;
      printf("globalid = %lld\n", globalId);
      printf("datasize = %lld\n", dataSize__);
      printf("globalid mod dataSize = %lld\n", globalId % dataSize__);
      printf("galaxy1 = %lld\n", galaxy1);
      printf("galaxy2 = %lld\n", galaxy2);
      exit(1);
    }

    float a1, a2, d1, d2;

    a1 = input[galaxy1] * (1 / 60.0f * PI / 180.0f);     // from arcmin to radian
    d1 = input[galaxy1 + 1] * (1 / 60.0f * PI / 180.0f); // from arcmin to radian

    a2 = input[galaxy2] * (1 / 60.0f * PI / 180.0f);     // from arcmin to radian
    d2 = input[galaxy2 + 1] * (1 / 60.0f * PI / 180.0f); // from arcmin to radian

    // angle between two galaxies
    // q12 = arccos(sin(d1)*sin(d2)+cos(d1)*cos(d2)*cos(a1-a2))
    float farg = sin(d1) * sin(d2) + cos(d1) * cos(d2) * cos(a1 - a2);

    if (farg > 1.0f)
      farg = 1.0f;
    if (farg < -1.0f)
      farg = -1.0f;

    float radian = acos(farg);
    float degree = radian * (180.0f / PI);

    int binIndex = (int)(degree / bin_width);
    if (binIndex < bin_count)
    {
      int globalBinIndex = (int)histogramIndex * bin_count + binIndex;
      if (globalBinIndex < histogramCount * bin_count)
      {
        // put in bin (SYNCHRONIZED ACTION)
        bin[globalBinIndex] += 1;
      }
    }
  }
}

void printResult(cudaError_t error)
{
  auto name = cudaGetErrorName(error);
  auto desc = cudaGetErrorString(error);
  auto name_str = std::string(name);
  std::string noError = "cudaSuccess";
  if (noError.compare(name_str) != 0)
  {
    std::cerr << "GPU error " << name << " : " << desc << std::endl;
  }
}

std::string format(std::string value, char thousandSep = ' ', char decimalSep = '.', char sourceDecimalSep = '.')
{
  int len = value.length();
  int negative = ((len && value[0] == '-') ? 1 : 0);
  int dpos = value.find_last_of(sourceDecimalSep);
  int dlen = 3 + (dpos == std::string::npos ? 0 : (len - dpos));

  if (dpos != std::string::npos && decimalSep != sourceDecimalSep)
  {
    value[dpos] = decimalSep;
  }

  while ((len - negative) > dlen)
  {
    value.insert(len - dlen, 1, thousandSep);
    dlen += 4;
    len += 1;
  }
  return value;
}

/**
 * @brief load float data from file
 *
 * @param array pointer to array
 * @param array_length array size (for a tuple)
 * @param file file name
 */
void load_csv_raw(float *array, int array_length, const char *file)
{
  FILE *file_handle;
  std::cout << "Loading float from file with pure C" << std::endl;
  file_handle = fopen(file, "r");
  if (file_handle == NULL)
  {
    std::cerr << "Error, file doesn't exists." << std::endl;
    return;
  }
  char char_buffer[180]; // char buffer on stack

  int i = 0;
  float a, d;
  while (fgets(char_buffer, 80, file_handle))
  {
    if (sscanf(char_buffer, "%f %f", &a, &d) == 2)
    {
      array[i] = a;
      i++;
      array[i] = d;
      i++;
    }
    else
    {
      std::cerr << "Cannot read line " << i << std::endl;
      exit(0);
    }
  }

  fclose(file_handle);
  std::cout << i << " floats have been read from file " << file << std::endl;
}

int main()
{
  std::cout << "Fabien CAYRE - GPU Programming - Abo Akademi - 2022" << std::endl;

  try
  {
    // load data from file

    bool test = true;
    bool gpu = true;


    auto start = std::chrono::high_resolution_clock::now();

    long long int dataSize = 100000;
    int arraySize = dataSize * floatCount;
    auto fake_data = (float *)malloc(sizeof(float) * arraySize);
    auto real_data = (float *)malloc(sizeof(float) * arraySize);

    load_csv_raw(fake_data, arraySize, "./random_galaxies.txt");
    load_csv_raw(real_data, arraySize, "./real_galaxies.txt");

    // allocate (input)
    //   size of one data set * number of float per set (2) * number of set
    // 100 000 * 2 * 2 * 3
    // 2 * 2 * 3 = 12
    long long int input_size = dataSize * floatCount * dataSetCount * histogramCount;
    //   sizeof(float) * full_dataSize
    auto input_array = (float *)malloc(sizeof(float) * input_size);

    // 6 sets to layout
    // D D D R R R
    // 0 1 2 3 4 5
    for (long long int setIndex = 0; setIndex < 6; setIndex++)
    {
      auto currentSet = real_data;
      if (setIndex >= 3)
      {
        currentSet = fake_data;
      }
      // loop from 0 to 100 000
      for (long long int i = 0; i < dataSize; i++)
      {
        // global offset
        // from 0 to 200 000
        long long int float_index = i * 2;
        long long int global_index = setIndex * dataSize * 2 + float_index;
        input_array[global_index] = currentSet[float_index];
        input_array[global_index + 1] = currentSet[float_index + 1];
      }
    }

    if (test)
    {
      // Array look in memory
      // D D D R R R
      // 200 000 float per set
      // 1.200 000 float in total
      std::cout << "Asserting value in right order" << std::endl;
      // first D
      for (int i = 0; i < dataSize; i++)
      {
        assert(real_data[i * 2] == input_array[i * 2]);
      }
      for (int i = 0; i < dataSize; i++)
      {
        assert(real_data[i * 2 + 1] == input_array[i * 2 + 1]);
      }
      // second D
      int offset = 2;
      for (int i = 0; i < dataSize; i++)
      {
        assert(real_data[i * 2] == input_array[offset * dataSize + i * 2]);
      }
      for (int i = 0; i < dataSize; i++)
      {
        assert(real_data[i * 2 + 1] == input_array[offset * dataSize + i * 2 + 1]);
      }
      // third D
      offset = 4;
      for (int i = 0; i < dataSize; i++)
      {
        assert(real_data[i * 2] == input_array[offset * dataSize + i * 2]);
      }
      for (int i = 0; i < dataSize; i++)
      {
        assert(real_data[i * 2 + 1] == input_array[offset * dataSize + i * 2 + 1]);
      }
      // first R
      offset = 6;
      for (int i = 0; i < dataSize; i++)
      {
        assert(fake_data[i * 2] == input_array[offset * dataSize + i * 2]);
      }
      for (int i = 0; i < dataSize; i++)
      {
        assert(fake_data[i * 2 + 1] == input_array[offset * dataSize + i * 2 + 1]);
      }
      // second R
      offset = 8;
      for (int i = 0; i < dataSize; i++)
      {
        assert(fake_data[i * 2] == input_array[offset * dataSize + i * 2]);
      }
      for (int i = 0; i < dataSize; i++)
      {
        assert(fake_data[i * 2 + 1] == input_array[offset * dataSize + i * 2 + 1]);
      }
      // third R
      offset = 10;
      for (int i = 0; i < dataSize; i++)
      {
        assert(fake_data[i * 2] == input_array[offset * dataSize + i * 2]);
      }
      for (int i = 0; i < dataSize; i++)
      {
        assert(fake_data[i * 2 + 1] == input_array[offset * dataSize + i * 2 + 1]);
      }
      std::cout << "All data is correctly layed out in memory." << std::endl;
    }
    m_debug;

    //   allocate (output)
    auto dd_bin_array = (unsigned long long int *)malloc(sizeof(long long int) * bin_count);
    auto dr_bin_array = (unsigned long long int *)malloc(sizeof(long long int) * bin_count);
    auto rr_bin_array = (unsigned long long int *)malloc(sizeof(long long int) * bin_count);
    memset(dd_bin_array, 0, sizeof(long long int) * bin_count);
    memset(dr_bin_array, 0, sizeof(long long int) * bin_count);
    memset(rr_bin_array, 0, sizeof(long long int) * bin_count);
    m_debug;
    auto cpu_bin_array = (unsigned long long int *)malloc(sizeof(long long int) * bin_count * histogramCount);
    for (int i = 0; i < bin_count * histogramCount; i++)
    {
      cpu_bin_array[i] = 0LLU;
    }

    // GPU (output)
    unsigned long long int *gpu_bin_array;
    float *gpu_input;
    if (gpu)
    {
      int cudaDeviceCount = 0;
      cudaGetDeviceCount(&cudaDeviceCount);
      std::cout << cudaDeviceCount << " device(s) support CUDA on this computer/laptot" << std::endl;
      if (cudaDeviceCount == 0)
      {
        std::cerr << "Cuda not supported" << std::endl;
        exit(1);
      }

      // GPU allocate (input)
      auto r = cudaMalloc((void **)&gpu_input, sizeof(float) * input_size);
      m_debug;
      printResult(r);
      r = cudaMemcpy(gpu_input, input_array, sizeof(float) * input_size, cudaMemcpyHostToDevice);
      m_debug;
      printResult(r);
      if (test)
      {
        auto cpu_test_input = (float *)malloc(sizeof(float) * input_size);
        r = cudaMemcpy(cpu_test_input, gpu_input, sizeof(float) * input_size, cudaMemcpyDeviceToHost);
        m_debug;
        printResult(r);
        // gpu layout
        // real real real fake fake fake
        // check set by set
        for (int i = 0; i < dataSize; i++)
        {
          assert(real_data[i * 2] == cpu_test_input[i * 2]);
        }
        for (int i = 0; i < dataSize; i++)
        {
          assert(real_data[i * 2 + 1] == cpu_test_input[i * 2 + 1]);
        }
        // second D
        int offset = 2;
        for (int i = 0; i < dataSize; i++)
        {
          assert(real_data[i * 2] == cpu_test_input[offset * dataSize + i * 2]);
        }
        for (int i = 0; i < dataSize; i++)
        {
          assert(real_data[i * 2 + 1] == cpu_test_input[offset * dataSize + i * 2 + 1]);
        }
        // third D
        offset = 4;
        for (int i = 0; i < dataSize; i++)
        {
          assert(real_data[i * 2] == cpu_test_input[offset * dataSize + i * 2]);
        }
        for (int i = 0; i < dataSize; i++)
        {
          assert(real_data[i * 2 + 1] == cpu_test_input[offset * dataSize + i * 2 + 1]);
        }
        // first R
        offset = 6;
        for (int i = 0; i < dataSize; i++)
        {
          assert(fake_data[i * 2] == cpu_test_input[offset * dataSize + i * 2]);
        }
        for (int i = 0; i < dataSize; i++)
        {
          assert(fake_data[i * 2 + 1] == cpu_test_input[offset * dataSize + i * 2 + 1]);
        }
        // second R
        offset = 8;
        for (int i = 0; i < dataSize; i++)
        {
          assert(fake_data[i * 2] == cpu_test_input[offset * dataSize + i * 2]);
        }
        for (int i = 0; i < dataSize; i++)
        {
          assert(fake_data[i * 2 + 1] == cpu_test_input[offset * dataSize + i * 2 + 1]);
        }
        // third R
        offset = 10;
        for (int i = 0; i < dataSize; i++)
        {
          assert(fake_data[i * 2] == cpu_test_input[offset * dataSize + i * 2]);
        }
        for (int i = 0; i < dataSize; i++)
        {
          assert(fake_data[i * 2 + 1] == cpu_test_input[offset * dataSize + i * 2 + 1]);
        }
        std::cout << "All set are in good order, and data transfert from GPU to CPU is working " << std::endl;

        free(cpu_test_input);
      }

      // GPU allocate (output)
      // GPU bin array is a one dimensional array representing the three histogram
      // allocate bin_count * 3 (number of histogram, DD, DR and RR)
      r = cudaMalloc((void **)&gpu_bin_array, sizeof(long long int) * bin_count * 3);
      m_debug;
      printResult(r);
      // GPU set
      if (test)
      {
        // test if array is clean
        for (int i = 0; i < bin_count * histogramCount; i++)
        {
          if (cpu_bin_array[i] != 0)
          {
            printf("At index %d, value is different than 0: %lld\n", i, cpu_bin_array[i]);
          }
        }
      }
      r = cudaMemcpy(gpu_bin_array, cpu_bin_array, sizeof(long long int) * bin_count * histogramCount, cudaMemcpyHostToDevice);
      m_debug;
      printResult(r);
      // load data to gpu
      struct cudaDeviceProp properties
      {
      };
      cudaGetDeviceProperties(&properties, 0);
      std::cout << "using " << properties.multiProcessorCount << " multiprocessors" << std::endl;
      std::cout << "max threads per processor: " << properties.maxThreadsPerMultiProcessor << std::endl;

      dim3 gridSize(4, 4096, 4096);
      dim3 blockSize(1024, 1, 1);

      std::cout << "Grid " << gridSize.x << " x " << gridSize.y << " x " << gridSize.z << std::endl;
      std::cout << "ThreadBlock " << blockSize.x << " x " << blockSize.y << " x " << blockSize.z << std::endl;

      // calculation
      m_debug;
      auto gpu_start = std::chrono::high_resolution_clock::now();
      kernelCompute<<<gridSize, blockSize>>>(gpu_bin_array, gpu_input, dataSize, input_size);

      // wait for GPU to finish calculation
      auto error = cudaPeekAtLastError();
      auto name = cudaGetErrorName(error);
      auto desc = cudaGetErrorString(error);
      std::cerr << "GPU error  " << name << " : " << desc << std::endl;
      r = cudaMemcpy(cpu_bin_array, gpu_bin_array, sizeof(long long int) * bin_count * histogramCount, cudaMemcpyDeviceToHost);
      m_debug;
      printResult(r);
      auto gpu_elapsed = std::chrono::high_resolution_clock::now() - gpu_start;
      long long gpu_milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(
                                       gpu_elapsed)
                                       .count();
      std::cout << "GPU code executed in " << gpu_milliseconds << " ms (" << gpu_milliseconds / 1000 << " s )" << std::endl;
    }
    else
    {

      int gridDimX = 1;
      // int gridDimY = std::cbrt(dataSize * dataSize) + 10;
      // int gridDimZ = std::cbrt(dataSize * dataSize) + 10;
      // int blockDimX = std::cbrt(dataSize * dataSize) + 10;

      long long int gridDimY = 4096;
      long long int gridDimZ = 4096;
      long long int blockDimX = 1024;

      long long int totalComputation = gridDimX * gridDimY * gridDimZ * blockDimX;
      long done = 0;
      std::cout << "Total of " << totalComputation << " computation" << std::endl;

      for (long long int lBlockIdx = 0; lBlockIdx < gridDimX; lBlockIdx++)
      {
        for (long long int lBlockIdy = 0; lBlockIdy < gridDimY; lBlockIdy++)
        {
          for (long long int lBlockIdz = 0; lBlockIdz < gridDimZ; lBlockIdz++)
          {
            for (long long int lThreadIdx = 0; lThreadIdx < blockDimX; lThreadIdx++)
            {
              cpuCompute(
                  lBlockIdx, lBlockIdy, lBlockIdz,
                  gridDimZ, blockDimX, gridDimX,
                  lThreadIdx,
                  cpu_bin_array, input_array, dataSize, input_size);
              done++;
            }
          }
          std::cout << (static_cast<float>(done) / static_cast<float>(totalComputation) * 100) << "%" << std::endl;
          // std::cout << "lBlockIdy = " << lBlockIdy << ", gridDimY = " << gridDimY << std::endl;
        }
      }
    }

    // save histograms
    m_debug;
    for (int i = 0; i < histogramCount; i++)
    {
      // iterate threw all histograms
      for (int y = 0; y < bin_count; y++)
      {
        int globalBinIndex = i * bin_count + y;
        unsigned long long int binValue = cpu_bin_array[globalBinIndex];
        switch (i)
        {
        case 0:
          if (y < 5)
          {
            std::cout << binValue << " bin value for dd" << std::endl;
          }
          dd_bin_array[y] = binValue;
          break;
        case 1:
          if (y < 5)
          {
            std::cout << binValue << " bin value for dr" << std::endl;
          }
          dr_bin_array[y] = binValue;
          break;
        case 2:
          if (y < 5)
          {
            std::cout << binValue << " bin value for rr" << std::endl;
          }
          rr_bin_array[y] = binValue;
          break;
        default: // impossible
          std::cerr << "Error at " << __LINE__ << " of file " << __FILE__ << std::endl;
          exit(1);
          break;
        }
      }
    }

    long long int total_amount = 0;
    total_amount += fi::write_csv("dd.csv", dd_bin_array, bin_count);
    total_amount += fi::write_csv("dr.csv", dr_bin_array, bin_count);
    total_amount += fi::write_csv("rr.csv", rr_bin_array, bin_count);
    std::cout << format(std::to_string(total_amount)) << " total computation";

    m_debug;
    // free memory
    // cpu
    free(dd_bin_array);
    free(dr_bin_array);
    free(rr_bin_array);
    free(input_array);
    free(cpu_bin_array);

    // gpu
    if (gpu)
    {
      cudaFree(gpu_input);
      cudaFree(gpu_bin_array);
    }

    auto elapsed = std::chrono::high_resolution_clock::now() - start;
    long long milliseconds = std::chrono::duration_cast<std::chrono::seconds>(
                                 elapsed)
                                 .count();
    std::cout << "Overall code executed in " << milliseconds << " s" << std::endl;
  }
  catch (std::exception &e)
  {
    std::cerr << "Exception caught : " << e.what() << std::endl;
  }

  return 0;
}
