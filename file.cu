//
// Created by fabien on 13/09/2022.
//

//
// Created by fabien on 13/09/2022.
//

#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <sstream>
#ifndef PI
#define PI 3.14159f
#endif
namespace fi
{

  template <typename Out>
  void split(const std::string &s, char delim, Out result)
  {
    std::istringstream iss(s);
    std::string item;
    while (std::getline(iss, item, delim))
    {
      if (!item.empty())
      {
        *result++ = item;
      }
    }
  }

  std::vector<std::string> split_str(const std::string &s, char delim)
  {
    std::vector<std::string> elems;
    split(s, delim, std::back_inserter(elems));
    return elems;
  }

  long file_size(std::string file_path)
  {
    std::ifstream testFile(file_path, std::ios::binary);
    const auto begin = testFile.tellg();
    testFile.seekg(0, std::ios::end);
    const auto end = testFile.tellg();
    const auto fsize = (end - begin);
    return fsize;
  }

  float arcmin_to_radian(float p)
  {
    return p * (PI / 10800.0f);
  }

  bool feq(float a, float b)
  {
    return fabs(a - b) < 0.001;
  }

  std::vector<std::tuple<float, float>> load_csv(const std::string filename)
  {
    std::vector<std::tuple<float, float>> data;
    std::ifstream input_file(filename);
    if (input_file.fail() || input_file.bad())
    {
      return data;
    }
    auto file_size = fi::file_size(filename);
    int i = 0;
    std::cout << "Value : " << filename << std::endl;
    for (std::string line; getline(input_file, line);)
    {
      std::replace(std::begin(line), std::end(line), '\t', ' ');
      std::replace(std::begin(line), std::end(line), '\r', ' ');
      std::replace(std::begin(line), std::end(line), '\n', ' ');
      auto splitted = fi::split_str(line, ' ');
      if (splitted.size() == 1)
      {
        continue;
      }
      std::string first = splitted.at(0);
      std::string second = splitted.at(1);

      auto a = std::stof(first);
      auto d = std::stof(second);

      data.push_back(
          std::make_tuple(a, d));

      // if (i < 5)
      // {
      //   std::cout << i << " => a=" << a << " arcmin, d=" << d
      //             << "arcmin | a=" << arcmin_to_radian(a) << "rad, d=" << arcmin_to_radian(d) << "rad" << std::endl;
      //   std::cout << "a=" << first << " | "
      //             << "d=" << second << " [file content] " << std::endl;
      //   i++;
      // }
    }

    return data;
  }

  unsigned long long int write_csv(const std::string filename, unsigned long long int *bin_data, int bin_count)
  {

    std::ofstream output_file(filename);
    if (output_file.fail())
    {
      return 0;
    }
    unsigned long long int total = 0;
    for (int i = 0; i < bin_count; i++)
    {
      auto output = std::to_string(bin_data[i]);
      output_file << output << std::endl;
      total += bin_data[i];
    }
    std::cout << "Recorded " << total << " bin on '" << filename << "'" << std::endl;

    return total;
  }

}
