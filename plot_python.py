import matplotlib.pyplot as plt
import numpy as np
import csv

bin_start = 0
bin_end = 180
bin_width = 0.25
bin_count = int((bin_end-bin_start)/bin_width)
bin_indeces = []
for i in range(bin_count):
    bin_indeces.append(float(i)*0.25)

with open("dd.csv", "r") as dd_file:
    with open("dr.csv", "r") as dr_file:
        with open("rr.csv", "r") as rr_file:
            dd_csv = csv.reader(dd_file)
            dr_csv = csv.reader(dr_file)
            rr_csv = csv.reader(rr_file)

            dd_bins = []
            for row in dd_csv:
                dd_bins.append(float(row[0]))

            dr_bins = []
            for row in dr_csv:
                dr_bins.append(float(row[0]))

            rr_bins = []
            for row in rr_csv:
                rr_bins.append(float(row[0]))

            print(dd_bins[:5])
            print(dr_bins[:5])
            print(rr_bins[:5])


            fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
            fig.suptitle('DD, RR, DR')
            ax1.bar(bin_indeces, dd_bins)
            ax1.set_title("DD")
            ax2.bar(bin_indeces, rr_bins)
            ax2.set_title("RR")
            ax3.bar(bin_indeces, dr_bins)
            ax3.set_title("DR")
            plt.show()

            # wi(q) = (DDi - 2*DRi + RRi)/RRi
            wi = []
            for i in range(bin_count):
                if rr_bins[i] == 0:
                    continue
                else:
                    wi.append((dd_bins[i] - 2*dr_bins[i] + rr_bins[i])/rr_bins[i])

            print("-"*20)
            print(wi[:4]) # first 4 values
            print(wi[-4:]) # last 4 values
            plt.bar(list(range(len(wi))), wi)
            plt.show()

